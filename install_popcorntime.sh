#!/usr/bin/env bash

echo "Instalando dependencias"
sudo apt update && sudo apt install libcanberra-gtk-module libgconf-2-4 curl -y

echo "Bajando Popcorn"
DownLink=$(curl -s https://get.popcorntime.sh/ | grep Linux-64 | sed -n 's/.*href="\([^"]*\).*/\1/p')
wget -c $DownLink

echo "Instalando en el sistema"
sudo mkdir /opt/popcorntime
sudo tar Jxf Popcorn-Time-* -C /opt/popcorntime
sudo ln -sf /opt/popcorntime/Popcorn-Time /usr/bin/Popcorn-Time
sudo sh -c 'cat  > /usr/share/applications/popcorntime.desktop << EOF
[Desktop Entry]
Version = 1.0
Type = Application
Terminal = false
Name = Popcorn Time
Exec = /usr/bin/Popcorn-Time
Icon = /opt/popcorntime/popcorn.png
Categories = Application;
EOF'


echo "Getting the icon"
sudo wget -O /opt/popcorntime/popcorn.png https://upload.wikimedia.org/wikipedia/commons/d/df/Pctlogo.png